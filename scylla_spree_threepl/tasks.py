# coding: utf8
from scylla import tasks
from scylla import orientdb
from scylla import graceful
import scylla_spree as spree
import scylla_threepl as threepl
from pprint import pprint


class ToThreePLOrderTask(threepl.ToThreePLOrderTask):
    key_field = 'id'


class ShipSpreeShipmentsTask(spree.ShipSpreeShipmentsTask):
    key_field = 'OrderId'

    def _get_shipment_id(self, obj, data=None):
        return obj.get('ReferenceNum')

    def _get_tracking_number(self, obj, data=None):
        return obj.get('RoutingInfo').get('TrackingNumber')


class LinkInventoryTask(tasks.ReflectTask):
    """Link 3PL inventory summaries to Spree locations
    """
    key_field = 'ItemId'

    def __init__(self, from_class, to_class, where=None, with_reflection=None):
        super(LinkInventoryTask, self).__init__(from_class, to_class, where=where, with_reflection=with_reflection)

        # manually specify task_id to avoid task name collision
        task_id = '{}{} > {}: link'.format(from_class[0], from_class[1], self.to_class)
        self.task_id = task_id

    def _process_response_record(self, obj):

        q = (
            "SELECT id "
            "FROM {spree_prefix}Variant "
            "WHERE sku = first(( "
                "SELECT Sku  "
                "FROM {tpl_prefix}Item "
                "WHERE ItemId={item_id} "
                "LIMIT 1 "
            ")).Sku "
            "LIMIT 1 "
        ).format(item_id=obj.get('ItemId'), tpl_prefix=self.from_module, to_type=self.to_type, spree_prefix=self.to_module)

        result = orientdb.execute(q)

        if len(result) == 0:
            print(u'Skipping {} {} as no {}Variant found'.format(self.from_class, obj.get('@rid'), self.to_module))
            return None

        q = (
            "SELECT @rid "
            "FROM {spree_prefix}{to_type} "
            "WHERE variant_id = {variant_id} "
            "LIMIT 1"
        ).format(item_id=obj.get('ItemId'), to_type=self.to_type, spree_prefix=self.to_module, variant_id=result[0].get('id'))

        result = orientdb.execute(q)
        rid = None

        if len(result) == 0:
            print(u'Skipping {} {} as no {}{} found'.format(self.from_class, obj.get('@rid'), self.to_module, self.to_type))
            return None

        to_rid = result[0].get('rid')
        self._save_response(obj, to_rid)

    def _save_response(self, from_obj, to_rid):
        reflection_type = 'Updated'

        # save the response to orientdb
        with graceful.Uninterruptable():

            # draw the edge
            orientdb.create_edge(from_obj['@rid'], to_rid,
                reflection_type, content=None)

            print(u'{} {} {}  –{}→  {} {}'.format(
                from_obj['@rid'],
                from_obj['@class'],
                from_obj.get('id', ''),
                reflection_type,
                self.to_class,
                to_rid,
            ))


class UpdateSpreeStockItemTask(spree.UpdateSpreeStockItemTask):
    """Updates shipments to a status of 'shipped' with tracking info
    """
    key_field = 'ItemId'

    def _step(self, after, options):
        self._cache = self._get_inventory_adjustment_cache()
        super(UpdateSpreeStockItemTask, self)._step(after, options)

    def _process_response_record(self, obj):
        """Hits a spree endpoint for the result
        """
        # create the record in spree
        if self.conversion:
            variant_id = obj.get('_linked_to').get('variant_id')

            # lazy load the cache if it isn't already created
            if not hasattr(self, '_cache'):
                self._cache = self._get_inventory_adjustment_cache()

            adjustment = self._cache.get(str(variant_id), 0)
            data = self.conversion(obj, adjustment)
        else:
            data = None

        is_update = obj.get('_linked_to', '') != ''

        spree_result = self._to_spree(obj, data)

        # save the spree response to orient and link it
        spree_rec = spree.records.SpreeRecord.factory(self.client, self._get_factory_type(), spree_result)
        self._save_response(obj, spree_rec, is_update, request=data)

    def _get_inventory_adjustment_cache(self):
        """Create a cache of the inventory that's been allocated in spree but not pushed into 3pl
        """

        shipments_q = (
            "SELECT expand(shipments) "
            "FROM {to_prefix}Order "
            "WHERE state = 'complete'  "
            "AND shipment_state != 'shipped' "
        )

        unsyncd_q = (
            "SELECT "
            "FROM ( "
               + shipments_q +
            ") "
            "WHERE out().out('Created')[@class instanceof '{from_prefix}Order'].size() = 0 "
            "AND out('Created')[@class instanceof '{from_prefix}Order'].size() = 0 "
        )

        manifest_q = (
            "SELECT manifest "
            "FROM ( "
                + unsyncd_q +
            ") "
            "UNWIND manifest "
        )

        q = (
            "SELECT manifest.variant_id AS variant_id, sum(manifest.quantity) AS qty "
            "FROM ( "
                + manifest_q +
            ") "
            "GROUP BY manifest.variant_id "
        ).format(to_prefix=self.to_module, from_prefix=self.from_module)

        results = orientdb.execute(q)
        cache = {}

        for result in results:
            variant_id = str(result.get('variant_id'))
            qty = int(result.get('qty'))
            cache[variant_id] = qty

        return cache

    def _get_url(self, obj, data=None):
        url = self.url_format.format(data.get('stock_location_id'), data.get('id'))
        data.pop('stock_location_id')
        data.pop('id')
        return url
